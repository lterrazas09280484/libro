/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Luis
 */
@Stateless
public class LibroBean implements LibroBeanRemote {
@PersistenceContext(name="Libreria-ejbPU")
EntityManager em;
Libro libro;
Collection <Libro> listalibros;
    
    @Override
    public void addLibros(String titulo, String autor, BigDecimal precio) {
    if(libro==null)    
    {
        libro = new Libro(titulo,autor,precio);
        em.persist(libro);
        libro=null;
    }
    }

    @Override
    public Collection<Libro> getAllLibros() {
        listalibros= em.createNamedQuery("Libro.findAll").getResultList();
        return listalibros;  
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public Libro buscaLibro(int id) {
    
        libro = em.find(Libro.class,id);
       
    return libro;
    }

    @Override
    public void actualizarLibro(Libro libro, String Titulo, String autor, BigDecimal precio) {
    if(libro!=null){
        System.out.println("Actualizando Libro");
        libro.setTitulo(Titulo);
        libro.setAutor(autor);
        libro.setPrecio(precio);
        em.merge(libro);
    }     
    }

    @Override
    public void eliminarlibro(int id) {
         libro=em.find(Libro.class, id);
        em.remove(libro);
        libro = null;
    }
}
