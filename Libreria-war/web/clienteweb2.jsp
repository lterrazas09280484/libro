<%-- 
    Document   : clienteweb2
    Created on : 06-nov-2014, 7:46:58
    Author     : Luis
--%>

<%@page import="stateless.LibroBeanRemote"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
   private LibroBeanRemote librocat = null;
   String s0,s1,s2,s3;
   Libro libro;
   Collection list;
   
   public void jspInit(){
       try{
           InitialContext context = new InitialContext();
           librocat = (LibroBeanRemote)
           context.lookup(LibroBeanRemote.class.getName());
           
           System.out.println("Cargando el Catalogo Bean"+librocat);
           }catch(Exception ex){
               System.out.println("Error:"+ex.getMessage());
               
           }
   }
       public void jspDestroy(){
           librocat=null;
       }
%>
<%
    try{
        s0=request.getParameter("id");
        s1=request.getParameter("t1");
        s2=request.getParameter("aut");
        s3=request.getParameter("precio");
        
        if(s0!=null && s1 != null && s2 !=null && s3 !=null)
        {
            Integer id = new Integer(s0);
            Double precio= new Double(s3);
            BigDecimal b=new BigDecimal(precio);
            libro = librocat.buscaLibro(id.intValue());
            librocat.actualizarLibro(libro,s1,s2,b);
            System.out.println("Registro actualizado");
            
        %>
        <p>
            <b>Registro Actualizado</b>
        </p>
        <%
        }
        list=librocat.getAllLibros();
        for(Iterator iter = list.iterator(); iter.hasNext();)
        {
            Libro elemento = (Libro)iter.next();
                    
        %>
        <br>
        <p>ID:<b><%= elemento.getId()%></b></p><br>
        <p>Titulo:<b><%= elemento.getTitulo()%></b></p><br>
        <p>Autor:<b><%= elemento.getAutor()%></b></p><br>
        <p>Precio:<b><%= elemento.getPrecio()%></b></p><br>
        <%
        }
        response.flushBuffer();
        %>
        Clic <a href="index.html">para regresar</a><br>
        <%
        }catch(Exception e){
            e.printStackTrace();
            
        }
%>
