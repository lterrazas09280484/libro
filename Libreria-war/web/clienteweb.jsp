<%-- 
    Document   : clienteweb
    Created on : 04-nov-2014, 7:54:13
    Author     : Luis
--%>

<%@page import="stateless.LibroBeanRemote"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
   private LibroBeanRemote librocat = null;
   String s1,s2,s3;
   Collection list;
   
   public void jspInit(){
       try{
           InitialContext context = new InitialContext();
           librocat = (LibroBeanRemote)
           context.lookup(LibroBeanRemote.class.getName());
           
           System.out.println("Cargando el Catalogo Bean"+librocat);
           }catch(Exception ex){
               System.out.println("Error:"+ex.getMessage());
               
           }
   }
       public void jspDestroy(){
           librocat=null;
       }
%>
<%
    try{
        s1=request.getParameter("t1");
        s2=request.getParameter("aut");
        s3=request.getParameter("precio");
        
        if(s1 != null && s2 !=null && s3 !=null)
        {
            Double precio= new Double(s3);
            BigDecimal b=new BigDecimal(precio);
            librocat.addLibros(s1,s2,b);
            System.out.println("Registro añadido");
            
        %>
        <p>
            <b>Registro dado de Alta</b>
        </p>
        <%
        }
        list=librocat.getAllLibros();
        for(Iterator iter = list.iterator(); iter.hasNext();)
        {
            Libro elemento = (Libro)iter.next();
                    
        %>
        <br>
        <p>ID:<b><%= elemento.getId()%></b></p>
        <p>Titulo:<b><%= elemento.getTitulo()%></b></p>
        <p>Autor:<b><%= elemento.getAutor()%></b></p>
        <p>Precio:<b><%= elemento.getPrecio()%></b></p>
        <%
        }
        response.flushBuffer();
        %>
        Clic <a href="index.html">para regresar</a><br>
        <%
        }catch(Exception e){
            e.printStackTrace();
            
        }
%>