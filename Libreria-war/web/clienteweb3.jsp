<%-- 
    Document   : clienteweb3
    Created on : 08-nov-2014, 14:37:57
    Author     : Luis
--%>

<%@page import="stateless.LibroBeanRemote"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private LibroBeanRemote librocat = null;
    String s0,s1,s2,s3;
    Collection list;
    Libro libro;
    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando Catalogo Bean" + librocat);
        } catch (Exception ex) {
            System.out.println("error " + ex.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>

<%
    try {
        s0 = request.getParameter("id");
        if (s0 != null) {
            Integer id = new Integer(s0);
            BigDecimal b;
            libro=librocat.buscaLibro(id.intValue());
            s1=libro.getTitulo();
            s2=libro.getAutor();
            Double p = libro.getPrecio().doubleValue();
            s3=p.toString();
%>
<script language="JavaScript" type="text/javascript">
function checkEli()
{
if (!confirm
('Libro Que Deseas Eliminar\n' + 'ID: <%=s0%>' + 
'\nTitulo: <%=s1%>' + 
'\nAutor: <%=s2%>' +
'\nPrecio: <%=s3%>'))
history.go(-1);
return ' ';
} 
document.writeln(checkEli());
</script>
<%
            librocat.eliminarlibro(id);
            System.out.println("Libro Eliminado");
%>


<p>Libro Eliminado</p>

<%
    }
    list = librocat.getAllLibros();
    for (Iterator iter = list.iterator(); iter.hasNext();) {
        Libro elemento = (Libro) iter.next();
%>
<br/>
 <br>
        <br>
        <p>ID:<b><%= elemento.getId()%></b></p><br>
        <p>Titulo:<b><%= elemento.getTitulo()%></b></p><br>
        <p>Autor:<b><%= elemento.getAutor()%></b></p><br>
        <p>Precio:<b><%= elemento.getPrecio()%></b></p><br>
        
<%
    }
    response.flushBuffer();
%>
</table><a href="index.html">Para Regresar</a>      
<%
    } catch (Exception ex) {
        ex.printStackTrace();
    }
%>
